from django.urls import path
from . import views


urlpatterns = [
    path('', views.OrderListView.as_view(), name="orders"),
    path('buy/<int:pk>/', views.OrderCreateView.as_view(), name="buy"),

]

from django.shortcuts import render
from django.views import generic
from django.shortcuts import redirect
from .models import Order
from product.models import Product


class OrderListView(generic.ListView):
    model = Order
    template_name = 'order/orders_list.html'
    context_object_name = 'orders'


class OrderCreateView(generic.CreateView):
    model = Order
    fields = ('product', 'user', 'price')
    success_url = 'orders'

    def get(self, request, *args, **kwargs):
        order = Order(user=request.user,
                      product=Product.objects.get(pk=self.kwargs['pk']),
                      price=Product.objects.get(pk=self.kwargs['pk']).price)
        order.save()
        return redirect('orders')

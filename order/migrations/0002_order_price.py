# Generated by Django 2.2.15 on 2020-08-14 20:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='price',
            field=models.FloatField(default=0, max_length=25),
        ),
    ]

from django.contrib import admin
from .models import Order


@admin.register(Order)
class AuthorAdmin(admin.ModelAdmin):
    list_display = ('product', 'user', 'price', 'created_time')

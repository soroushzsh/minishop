from django.contrib.auth.models import User
from django.db import models
from product.models import Product


class Order(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    price = models.FloatField(max_length=25, default=0)
    created_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.id}- Order for {self.product} by {self.user} at {self.created_time}"
